{pkgs, ...}:
with builtins; {
  home.packages = with pkgs; [
    (pkgs.pass.withExtensions (ext: [ext.pass-otp]))
    pkgs.zbar
    pkgs.wl-clipboard
    pkgs.yad
    pkgs.ydotool
  ];
}
