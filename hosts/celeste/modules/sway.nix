{
  pkgs,
  lib,
  ...
}:
with lib; let
  swwwPkg = pkgs.callPackage ../../../pkgs/swww.nix {};
in {
  home.packages = [
    swwwPkg
  ];

  # TODO: Write sway config file
  xdg.configFile.sway = {
    source = ../configs/sway;
    recursive = true;
  };
}
