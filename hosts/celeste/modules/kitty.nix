{
  programs.kitty = {
    enable = true;
    settings = {
      shell_integration = "no-cursor";
      # Fonts
      font_family = "Iosevka Nerd Font";
      font_size = 30;

      # Cursor
      cursor_shape = "underline";
      cursor_underline_thickness = 8;

      # Colors
      background = "#181616";
      foreground = "#c5c9c5";
      selection_background = "#2D4F67";
      selection_foreground = "#C8C093";
      url_color = "#72A7BC";
      cursor = "#C8C093";

      # Tabs
      active_tab_background = "#12120f";
      active_tab_foreground = "#C8C093";
      inactive_tab_background = "#12120f";
      inactive_tab_foreground = "#a6a69c";
      #tab_bar_background #15161E

      # normal
      color0 = "#0d0c0c";
      color1 = "#c4746e";
      color2 = "#8a9a7b";
      color3 = "#c4b28a";
      color4 = "#8ba4b0";
      color5 = "#a292a3";
      color6 = "#8ea4a2";
      color7 = "#C8C093";

      # bright
      color8 = "#a6a69c";
      color9 = "#E46876";
      color10 = "#87a987";
      color11 = "#E6C384";
      color12 = "#7FB4CA";
      color13 = "#938AA9";
      color14 = "#7AA89F";
      color15 = "#c5c9c5";

      # extended colors
      color16 = "#b6927b";
      color17 = "#b98d7b";

      tab_bar_min_tabs = 1;
      tab_bar_edge = "bottom";
      tab_bar_style = "powerline";
      tab_powerline_style = "slanted";
      tab_title_template = "{title}{' :{}:'.format(num_windows) if num_windows > 1 else ''}";
      active_tab_font_style = "bold";
      inactive_tab_font_style = "normal";
    };
  };
}
