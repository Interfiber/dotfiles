return lib:makeModule({
	name = "gitsigns",
	enabled = true,
	onActivate = function()
		require('gitsigns').setup({})
	end,
})
