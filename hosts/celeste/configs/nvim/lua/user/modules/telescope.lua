return lib:makeModule({
	name = "telescope",
	enabled = true,

	onActivate = function()
		require("telescope").setup({
			extensions_list = { "themes", "terms" },
		})
	end,
})
