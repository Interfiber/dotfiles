return lib:makeConfig({
	editor = {
		colorscheme = lib:makeColorscheme({
			name = "kanagawa", -- colorscheme name
			background = "dark", -- vim.opt.background
			setupType = "function", -- default or function
			setup = function()
				require("kanagawa").setup({
					compile = false,
					commentStyle = { italic = true },
					functionStyle = { italic = true },
					dimInactive = true, -- dim inactive window `:h hl-NormalNC`
					theme = "dragon",
					overrides = function(colors)
						local theme = colors.theme
						return {
							TelescopeTitle = { fg = theme.ui.special, bold = true },
							TelescopePromptNormal = { bg = theme.ui.bg_p1 },
							TelescopePromptBorder = { fg = theme.ui.bg_p1, bg = theme.ui.bg_p1 },
							TelescopeResultsNormal = { fg = theme.ui.fg_dim, bg = theme.ui.bg_m1 },
							TelescopeResultsBorder = { fg = theme.ui.bg_m1, bg = theme.ui.bg_m1 },
							TelescopePreviewNormal = { bg = theme.ui.bg_dim },
							TelescopePreviewBorder = { bg = theme.ui.bg_dim, fg = theme.ui.bg_dim },
							Pmenu = { fg = theme.ui.shade0, bg = theme.ui.bg_p1 }, -- add `blend = vim.o.pumblend` to enable transparency
							PmenuSel = { fg = "NONE", bg = theme.ui.bg_p2 },
							PmenuSbar = { bg = theme.ui.bg_m1 },
							PmenuThumb = { bg = theme.ui.bg_p2 },
						}
					end,
					background = {
						dark = "dragon",
						light = "lotus",
					},
				})

				vim.cmd("colorscheme kanagawa")
			end,
		}),
		keybinds = lib:makeEditorKeybinds({
			{ binding = "<Leader>f", action = ":Telescope find_files" },
			{ binding = "<Leader>w", action = ":bd" },
			{ binding = "<Leader>t", action = ":Neotree toggle" },
			{ binding = "<Leader>h", action = ":bprev" },
			{ binding = "<Leader>l", action = ":bnext" },
			{ binding = "<Leader>g", action = ":Telescope grep_string" },
			{ binding = "gr",        action = ":Lspsaga rename" },
			{ binding = "gh",        action = ":Lspsaga lsp_finder" },
			{ binding = "K",         action = ":Lspsaga hover_doc ++keep" },
			{ binding = "tt",        action = ":Lspsaga term_toggle" },
			{ binding = "gt",        action = ":Lspsaga show_workspace_diagnostics" },
			{
				binding = "gd",
				action = function()
					vim.lsp.buf.definition()
				end,
			},
			{
				binding = "gv",
				action = function()
					vim.lsp.buf.code_action()
				end,
			},
		}),
		settings = lib:makeEditorSettings({
			lineNumbers = true,
			relativeLineNumbers = true,
			termGuiColors = true,
			tabSize = 3,
			leader = " ",
		}),
	},
	languages = lib:makeLanguageServers({
		"clangd",
		"rust_analyzer",
		"tsserver",
		"lua_ls",
		"hls",
		"zls",
	}),
	modules = require("user.modules"),
	packages = lib:makePackageList({
		utils:makeColorschemePackages(),
		"akinsho/bufferline.nvim",    -- Tab bar
		"nvim-lualine/lualine.nvim",  -- Status line
		"goolord/alpha-nvim",         -- Startup screen
		"nvim-tree/nvim-web-devicons", -- Nerd font icon support
		{
			"nvim-telescope/telescope.nvim", -- Fuzzy finder
			dependencies = { "nvim-lua/plenary.nvim" },
		},
		"nvim-treesitter/nvim-treesitter", -- Pretty syntax highlights
		{
			"nvim-neo-tree/neo-tree.nvim",
			branch = "v2.x",
			dependencies = {
				"nvim-lua/plenary.nvim",
				"nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
				"MunifTanjim/nui.nvim",
			},
		},
		"folke/todo-comments.nvim",          -- TODO, HACK, BUG, FIXME, etc comments
		"lukas-reineke/lsp-format.nvim",     -- Auto format files with the LSP server on save
		"lewis6991/gitsigns.nvim",           -- Git decorations
		{ "j-hui/fidget.nvim", tag = "legacy" }, -- LSP server progress
		utils:makeLanguageServerPackages(),
	}),
})
