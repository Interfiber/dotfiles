{pkgs, ...}:
with builtins; {
  programs.zsh.enable = true;
  users.users.interfiber = {
    isNormalUser = true;
    description = "Interfiber";
    extraGroups = ["networkmanager" "wheel" "vboxusers" "video"];
    shell = pkgs.zsh;
  };
}
